from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.


'''''
def check_user(request):
    check = Receipt.objects.filter(purchaser=request.user)
    context = {
        "check_user": check,
    }
    return render(request, "receipts/receipts.html", context)
'''''


@login_required(login_url='/accounts/login/')
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipt_list': receipt,
    }
    return render(request, "receipts/receipts.html", context)


@login_required(login_url='/accounts/login/')
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, 'receipts/create.html', context)


@login_required(login_url='/accounts/login/')
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'categories': category,
    }
    return render(request, "receipts/category_list.html", context)


@login_required(login_url='/accounts/login/')
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        'accounts': account,
    }
    return render(request, 'receipts/account_list.html', context)


@login_required(login_url='/accounts/login/')
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, 'categories/create.html', context)


@login_required(login_url='/accounts/login/')
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, 'accounts/create.html', context)
